import {Framework, Utils} from "floose";
import Driver = Framework.Driver;

export interface ModbusDriverConfig {
    host: string;
    port: number;
    unitId: number;
}

export declare class ModbusDriver extends Driver {
    readonly configurationValidationSchema: Utils.Validation.Schema;
    init(config: ModbusDriverConfig): Promise<void>;
    readCoils(address: number, quantity: number): Promise<boolean[]>;
    readDiscreteInputs(address: number, quantity: number): Promise<boolean[]>;
    readHoldingRegisters(address: number, quantity: number): Promise<Buffer[]>;
    readInputRegisters(address: number, quantity: number): Promise<Buffer[]>;
    writeSingleCoil(address: number, value: boolean): Promise<void>;
    writeSingleHoldingRegister(address: number, value: Buffer | number): Promise<void>;
    writeMultipleCoils(address: number, values: boolean[]): Promise<void>;
    writeMultipleHoldingRegisters(address: number, values: (Buffer| number)[]): Promise<void>;
}